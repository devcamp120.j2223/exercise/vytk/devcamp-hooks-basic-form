import { useEffect, useState } from "react"

function Form(){
    const [firstname,setFirstname]=useState(localStorage.getItem("firstname")||"");
    const [lastname,setLastname]=useState(localStorage.getItem("lastname")||"");

    const firstnameChangeHanler=(event)=>{
        setFirstname(event.target.value);
    }
    const lastnameChangeHanler=(event)=>{
        setLastname(event.target.value);
    }

    useEffect(()=>{
        localStorage.setItem('firstname',firstname);
        localStorage.setItem('lastname',lastname);
    })

    return(
        <div>
            <div><input value={firstname} onChange={firstnameChangeHanler} placeholder="Firstname"/></div>
            <div><input value={lastname} onChange={lastnameChangeHanler} placeholder="Lastname"/></div>
            <p>{firstname}{lastname}</p>
        </div>
    )
}
export default Form;